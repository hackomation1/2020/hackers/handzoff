# 2. Embedded solution

HandZoff is supposed to open your door for you after putting your card or tag in front of the device. Also opening the door for you after you've connected to the WiFi

## 2.1 Objectives

We started with putting the components together to win time Because we participated late. Then went on and did some research on what we actually wanted. After that it was time to work on the code and the rest of the tasks. 
Provide background and reasoning what needs to be solved here, why this solution was choosen and what steps need to be taken


## 2.3 Steps taken
1. Downloaded all the neccessary libraries and boards on the Arduino IDE app
2. Compiled and Uploaded our then code via a USB cable to the setup
3. We measured the components so we wouldn't make mistakes for the enclosure
4. Then at the end everything was put in the enclosure and ready for the demonstration

## 2.4 Testing & Problems

First, We encountered problems with the code because we were not used to the components we were utilizing. We managed to get the code to work after getting many errors.

Secondly, After I put all of my components together, the code did not seem to want to communicate with them. This was due to the fact that some of the components had faulty. I solved that by replacing the components that had problems.

I then went on to make a dashboard to monitor the activity of the ESP32. Because I had some diffulties in making it, I went ahead and attempted to make an app.


## 2.5 Proof of work

< Add video and/or photos of your tests and proof it works!>

## 2.6 Files & Code

<Add your embedded code and other relevant documents in a ZIp file under the /files folder and link here.>

## 2.7 References & Credits

https://dl.espressif.com/dl/package_esp32_index.json
