# 4. Business & Marketing


## 4.1 Objectives
The ongoing pandemic has made people hesitate to touch surfaces because they fear getting infected by the corona virus. Because people can't see the virus particles, they resort to always disinfecting areas. The amount of tourist has decreased drastically. Why? Also because of the Covid-19. And that's where HandZoff steps in to safe the day. With HandZoff you won't need to disinfect your door because you won't be touching it. Not only can residences use this smart lock but lodges, hotels, resorts can make use of it. 

## 4.3 Ananlyze your market & segments
**Customer Segments**

- Commercial sector
- Residential Institution & Government
- Industrial sector

**Value proposition**
- Customizable
- Anti-Theft 
- Durability

**Channels**
- Direct visitation
- Social Media
- Promotion
- TV and Radio 
- Digital marketing


## 4.4 Build your business canvas

https://canvanizer.com/canvas/wOG6sMw030MJs

## 4.5 Build your pitch deck

[https://docs.google.com/presentation/d/1kGmbPjxwC3Gu29C5-0-5wQ469JGJeSBqQsAuNKxA0QM/edit#slide=id.gadbed6c9be_0_52]

## 4.6 Make a project Poster
Open images