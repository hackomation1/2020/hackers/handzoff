# 1. Project Title: HandZoff

HandZoff is a contactless smart lock
It is supposed to sustain tourism and prevent Covid-19 at the same time.

## Overview & Features


1. **RFID** (MFRC522)
2. **RFID** tag and card
3. **ESP32** with builtin OLED
4. **Jumpers** 
5. **Solenoid Lock (12V)**
6. **Relay**
7. **Breadboard** (solderless)
8. **12 Volt Feed**

## Demo / Proof of work


< Fotos, video of your working project as proof of work>

## Deliverables

< The deliverables of the project, so we know when it is complete>
- Project poster
- Proof of concept working
- Product pitch deck

## Project landing page/website (optional)

https://handzoffyourlife.glideapp.io/

## the team & contact


Manischa Adjako (5977481227)
1. Coding
2. 3D Design
3. Presentation
4. App Design
5. Project Pitch
6. Documentation

Ghilles Dipotroeno (5978111788):
1. Slides
2. Poster
3. Presentation
4. Documentation

Shulaika La Cruise (5978785591): 
1. Business model canvas
