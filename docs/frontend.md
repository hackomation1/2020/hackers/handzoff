# 3. Front-End & Platform
Monitoring all activities of HandZoff in its app to guarantee you your safety and privacy.

## 3.1 Objectives
It is so that people can enter your residence but you don't actually know who. HandZoff can be of your assistance because it will show who had access or who had been denied it.


## 3.3 Steps taken
1. Test or Use the Blink example on the ESP32
2. Including the neccessary libraries and boards to the codes
3. Making spreadsheets for the App


## 3.4 Testing & Problems
1. Using various already made codes to see if they'll live up t our expectations
(Had problems with the libraries used and the occurring errors)
2. Using the toggle method to observe the ESP32
(Most dashboards didn't seem to be what we were looking for so we tried making an app)

